// Functions
function printUserInfo(firstName, lastName, age, hobbies, workAddress) {
	console.log("First Name: "+firstName);
	console.log("Last Name: "+lastName);
	console.log("Age: "+age);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log("Work Adress:");
	console.log(workAddress);
	console.log(firstName+ " is " + age + " of age.")
	console.log("This was printed inside of the function");
	console.log(hobbies);
	console.log("This was printed inside of the function");
	console.log(workAddress);
}


// Variables
let fname = "John";
let lname = "Smith";
let a = 30;
let hobby = ["Biking", "Mountain Climbing", "Swimming"];
let address = {"houseNumber":"32", "street":"Washington", "city":"Lincoln", "state":"Nebraska"};

printUserInfo(fname, lname, a, hobby, address);